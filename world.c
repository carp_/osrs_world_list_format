#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/stat.h>

#include "world.h"

static const size_t HEADER_SZ = sizeof(world_hdr);
static const size_t MAX_WORLD_STRLEN = 26;  // A guess

const char *get_world_type(world_entry *w)
{
    switch (w->metadata) {
        case LMS_COMP:
            return "Last Man Standing Competitive";
        case PVP_HRISK:
            return "PVP High Risk World";
        case HIGH_RISK:
            return "High Risk World";
        case LMS_CASUAL:
            return "Last Man Standing Casual";
        case DM_SEASONAL:
            return "Deadman Seasonal";
        case DM_REGULAR:
            return "Deadman";
        case PVP_REGULAR:
            return "PVP World";
        case BOUNTY_HUNTER:
            return "Bounty Hunter World";
        case TOTAL_2000:
            return "Total 2000";
        case TOTAL_1750:
            return "Total 1750";
        case TOTAL_1500:
            return "Total 1500";
        case TOTAL_1250:
            return "Total 1250";
        case F2P:
            return "Free to Play";
        case P2P:
            return "Pay to Play";
        default:
            return "Unknown";
    }
}

const char *get_world_loc(world_entry *w)
{
    switch (w->location) {
        case US_WORLD:
            return "US";
            break;
        case UK_WORLD:
            return "UK";
            break;
        case GERMAN_WORLD:
            return "German";
            break;
        default:
            return "N/A";
    }
}

world_hdr *get_world_hdr(uint8_t *list)
{
    world_hdr *hdr = malloc(sizeof(*hdr));
    memcpy(hdr, list, sizeof(*hdr));
    hdr->unpadded_size = htonl(hdr->unpadded_size);
    hdr->world_cnt = htons(hdr->world_cnt);

    return hdr;
}

void cleanup_world_list(world_list *wl)
{
    for (size_t i = 0; i < wl->hdr->world_cnt; i++) {
        free(wl->worlds[i].title);
        free(wl->worlds[i].link);
    }
    free(wl->worlds);
    free(wl->hdr);
    free(wl);
}

world_list *get_world_list(uint8_t *list)
{
    uint8_t *ptr = list + HEADER_SZ;
    world_hdr *hdr = get_world_hdr(list);
    world_entry *worlds = malloc(sizeof(*worlds) * hdr->world_cnt);

    for (size_t i = 0; i < hdr->world_cnt; i++) {
        worlds[i].number = htons(*((uint16_t *) ptr));
        ptr += sizeof(worlds[i].number);

        worlds[i].metadata = htonl(*((uint32_t *) ptr));
        ptr += sizeof(worlds[i].metadata);

        worlds[i].link = strdup((char *) ptr);
        ptr += strnlen(worlds[i].link, MAX_WORLD_STRLEN) + 1;

        worlds[i].title = strdup((char *) ptr);
        ptr += strnlen(worlds[i].title, MAX_WORLD_STRLEN) + 1;

        worlds[i].location = *ptr;
        ptr += sizeof(worlds[i].location);

        worlds[i].population = htons(*((uint16_t *) ptr));
        ptr += sizeof(worlds[i].population);
    }

    world_list *wl = malloc(sizeof(*wl));
    wl->hdr = hdr;
    wl->worlds = worlds;

    return wl;
}

uint8_t *read_worldlist_file(char *filename)
{
    struct stat info;
    int ret = stat(filename, &info);

    if (ret == -1) {
        return NULL;
    }

    FILE *wl = fopen(filename, "rb");
    if (wl == NULL) {
        return NULL;
    }

    uint8_t *data = malloc(info.st_size);

    fread(data, 1, info.st_size, wl);
    fclose(wl);

    return data;
}
