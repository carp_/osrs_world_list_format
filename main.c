#include <stdint.h>
#include <stdlib.h>

#include "world.h"

static uint32_t get_count(world_list *wl, WORLD_LOC loc);

int main(void)
{
    uint8_t *list = read_worldlist_file("./world_lists/current_slr.ws");
    if (list == NULL) {
        fprintf(stderr, "Unable to read world list file.\n");
        return 1;
    }

    world_list *wl = get_world_list(list);
    printf("World list size: %d bytes\n", wl->hdr->unpadded_size);

    for (size_t i = 0; i < wl->hdr->world_cnt; i++) {
        printf("W%d (%-25s) %6s %s %4d players -- %s\n", 
                wl->worlds[i].number,
                wl->worlds[i].link, 
                get_world_loc(&wl->worlds[i]),
                wl->worlds[i].metadata & P2P ? "P2P": "F2P",
                wl->worlds[i].population,
                get_world_type(&wl->worlds[i]));
    }

    // Examples

    uint32_t total  = get_count(wl, ALL);
    uint32_t german = get_count(wl, GERMAN_WORLD);
    uint32_t us     = get_count(wl, US_WORLD);
    uint32_t uk     = get_count(wl, UK_WORLD);

    puts("");
    printf("Total Players : %5d\n", total);
    printf("German Players: %5d\n", german);
    printf("US Players    : %5d\n", us);
    printf("UK Players    : %5d\n", uk);
    puts("");

    printf("%% US Players    : %.02f%%\n", ((float) us / (float) total) * 100.0);
    printf("%% German Players: %.02f%%\n", ((float) german / (float) total) * 100.0);
    printf("%% UK Players    : %.02f%%\n", ((float) uk / (float) total) * 100.0);
    puts("");

    free(list);
    cleanup_world_list(wl);
}

static uint32_t get_count(world_list *wl, WORLD_LOC loc)
{
    uint32_t total = 0;

    for (size_t i = 0; i < wl->hdr->world_cnt; i++) {
        if (loc == ALL || loc == wl->worlds[i].location) {
            total += wl->worlds[i].population;
        }
    }

    return total;
}
