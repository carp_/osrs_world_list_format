# OSRS World List File Format

This repo contains documentation and examples related to the Oldschool Runescape world list file, which can be found here:

`http://oldschool.runescape.com/slr.ws`

See `osrs_wlist.md` or `osrs_wlist.pdf` for _nearly_ complete file format documentation.