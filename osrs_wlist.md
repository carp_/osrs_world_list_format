# OSRS World List File Format

This binary file can be found at the following url: 
`http://oldschool.runescape.com/slr.ws`

The reference to this link can be located in jav\_config: 
`http://oldschool.runescape.com/jav_config`

The world list contains a header and multiple world entries. It is padded with zeroes to be exactly 5000 bytes long. Multibyte members are in big endian format.

The header of the world list is 6 bytes:

```c
typedef struct world_hdr {
    uint32_t file_size; // Size of file, excluding padding
    uint16_t world_cnt; // Number of world entries in the file.
} __attribute__((packed)) world_hdr;
```

This header is followed by variable-length world entries.
Each world entry is composed of the following members:

```c
typedef struct world_entry {
    uint16_t number;        // Ex: 365 (For oldschool65.runescape.com)
    uint32_t metadata;
    char *link;             // null terminated, variable length
    char *title;            // null terminated, variable length
    uint8_t location;       // World location (US, UK, etc)
    uint16_t population;    // World player count
} world_entry;
```

`metadata` is still not well understood. So far, it seems the four bytes identify:

- Whether a world is f2p or p2p
- Whether a world has special requirements (Total req, pvp, etc)

The following 4 byte codes are currently known:

```c
typedef enum METADATA {                                                                   
    LMS_COMP      = 0x00004001UL,                                                              
    PVP_HRISK     = 0x00000405UL,                                                              
    HIGH_RISK     = 0x00000401UL,                                                              
    LMS_CASUAL    = 0x00004000UL,                                                              
    DM_SEASONAL   = 0x60000001UL,                                                               
    DM_REGULAR    = 0x20000001UL,                                                              
    PVP_REGULAR   = 0x00000005UL,                                                              
    BOUNTY_HUNTER = 0x00000021UL,                                                               
    TOTAL_2000    = 0x00040001UL,                                                              
    TOTAL_1750    = 0x10000001UL,                                                              
    TOTAL_1500    = 0x00000081UL,                                                              
    TOTAL_1250    = 0x08000001UL,                                                              
    F2P           = 0x00000000UL,                                                              
    P2P           = 0x00000001UL,                                                              
} METADATA;
```

The F2P and P2P METADATA members can be used as a mask for detecting whether a world is F2P or P2P:

```c
world_entry *world;	// initialized as w301, a f2p world

if (world->metadata & F2P) {
    puts("World is f2p!");
}
```

It's difficult to say whether this idea is correct, but it's a start.

`location` is a constant, which is one of the following:

```c
typedef enum WORLD_LOC = {
	GERMAN_WORLD = 7,
	UK_WORLD     = 1,
	US_WORLD     = 0,
} WORLD_LOC;
```

## Accessing different list layouts

Supposedly, the list can be presorted by the server. This does not appear to work, but is listed in the jav_config nonetheless. Anyhow, here are the following list options:

```
Sort by world (w/W):
http://oldschool.runescape.com/slr?order=wMLPA
http://oldschool.runescape.com/slr?order=WMLPA

Sort by population (p/P):
http://oldschool.runescape.com/slr?order=pMLWA
http://oldschool.runescape.com/slr?order=PMLWA

Sort by location (l/L):
http://oldschool.runescape.com/slr?order=lMPWA
http://oldschool.runescape.com/slr?order=LMPWA

Sort by subscription (m/M):
http://oldschool.runescape.com/slr?order=mLPWA
http://oldschool.runescape.com/slr?order=MLPWA

Sort by activity (a/A):
http://oldschool.runescape.com/slr?order=aMLPW
http://oldschool.runescape.com/slr?order=AMLPW
```

The first letter indicates whether the list is ordered by highest (lowercase) or lowest (uppercase). It may also be possible to rearrange the letters to obtain a custom sorted list.