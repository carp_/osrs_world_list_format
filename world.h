#include <stdio.h>
#include <stdint.h>

typedef enum METADATA {
    LMS_COMP      = 0x00004001UL,
    PVP_HRISK     = 0x00000405UL,
    HIGH_RISK     = 0x00000401UL,
    LMS_CASUAL    = 0x00004000UL,
    DM_SEASONAL   = 0x60000001UL,
    DM_REGULAR    = 0x20000001UL,
    PVP_REGULAR   = 0x00000005UL,
    BOUNTY_HUNTER = 0x00000021UL,
    TOTAL_2000    = 0x00040001UL,
    TOTAL_1750    = 0x10000001UL,
    TOTAL_1500    = 0x00000081UL,
    TOTAL_1250    = 0x08000001UL,
    F2P           = 0x00000000UL,
    P2P           = 0x00000001UL,
} METADATA;

typedef enum WORLD_LOC {
    GERMAN_WORLD = 7,
    UK_WORLD     = 1,
    US_WORLD     = 0,
    ALL          = 10,  // NOT part of the world list file format.
} WORLD_LOC;

typedef struct world_hdr {
    uint32_t unpadded_size;
    uint16_t world_cnt;
} __attribute__((packed)) world_hdr;

typedef struct world_entry {
    uint16_t number;
    uint32_t metadata;
    char *link;
    char *title;
    uint8_t location;
    uint16_t population;
} world_entry;

typedef struct world_list {
    world_hdr *hdr;
    world_entry *worlds;
} world_list;

uint8_t *read_worldlist_file(char *filename);
uint32_t get_player_count(world_list *wl);
struct world_hdr *get_world_hdr(uint8_t *list);
struct world_list *get_world_list(uint8_t *list);
void print_world_list(world_list *wl);
void cleanup_world_list(world_list *wl);
const char *get_world_loc(world_entry *w);
const char *get_world_type(world_entry *w);
